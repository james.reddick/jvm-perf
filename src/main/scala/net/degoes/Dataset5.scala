package net.degoes

import scala.collection.mutable.Map as MutableMap
import zio.Chunk
import scala.annotation.switch
import java.util.function.LongBinaryOperator

object dataset5 {

  final case class Field(name: String)

  sealed trait Value
  object Value {
    final case class Text(value: String)    extends Value
    final case class Integer(value: Long)   extends Value
    final case class Decimal(value: Double) extends Value
    case object NA                          extends Value
  }

  final case class Row(map: Map[String, Value]) {
    def apply(field: Field): Value = map(field.name)
  }

  sealed abstract class ByteCode(val tag: Int)

  object ByteCode {
    case class Get(field: Field, reg: Register) extends ByteCode(0)
    case class Push(value: Register)            extends ByteCode(1)
    case class Pop(value: Register)             extends ByteCode(2)
    case object Swap                            extends ByteCode(3)
    case object Add                             extends ByteCode(4)
    case object Sub                             extends ByteCode(5)
    case object Mul                             extends ByteCode(6)
    case object Div                             extends ByteCode(7)
  }

  type Register = Int
  val R0: Register = 0
  val R1: Register = 1

  sealed trait Binary

  object Binary {
    case object ADD extends Binary
    case object SUB extends Binary
    case object MUL extends Binary
    case object DIV extends Binary
  }

  sealed trait Op {
    import Op.*

    private[this] var _compiled: Compiled = null

    def *(that: Op): Op =
      Mul(this, that)

    def /(that: Op): Op =
      Div(this, that)

    def +(that: Op): Op =
      Add(this, that)

    def -(that: Op): Op =
      Sub(this, that)

    private def _compile: Compiled = {
      def calculateStackSize(bc: Chunk[ByteCode]) =
        bc.foldLeft(0 -> 0) { case ((mss, css), bc) =>
          bc match {
            case ByteCode.Push(_) => (mss max (css + 1)) -> (css + 1)
            case ByteCode.Pop(_)  => mss                 -> (css - 1)
            case _                => mss                 -> css
          }
        }._1

      def optimise(bc: Chunk[ByteCode]): Chunk[ByteCode] = {
        def go(bc: List[ByteCode]): List[ByteCode] =
          bc match {
            case ByteCode.Push(R0) ::
                ByteCode.Get(f2, R0) ::
                ByteCode.Swap ::
                ByteCode.Pop(R0) ::
                tail =>
              ByteCode.Get(f2, R1) :: go(tail)
            case otherwise :: tail =>
              otherwise :: go(tail)
            case Nil =>
              Nil
          }

        Chunk.from(go(bc.toList))
      }

      def loop(op: Op): Chunk[ByteCode] =
        op match {
          case x: GetLong =>
            Chunk(ByteCode.Get(x.field, R0))
          case _: GetDouble =>
            Chunk.empty // TODO
          case Add(left, right) =>
            loop(left)
              ++ Chunk(ByteCode.Push(R0))
              ++ loop(right)
              ++ Chunk(ByteCode.Swap, ByteCode.Pop(R0), ByteCode.Add)
          case Sub(left, right) =>
            loop(left)
              ++ Chunk(ByteCode.Push(R0))
              ++ loop(right)
              ++ Chunk(ByteCode.Swap, ByteCode.Pop(R0), ByteCode.Sub)
          case Mul(left, right) =>
            loop(left)
              ++ Chunk(ByteCode.Push(R0))
              ++ loop(right)
              ++ Chunk(ByteCode.Swap, ByteCode.Pop(R0), ByteCode.Mul)
          case Div(left, right) =>
            loop(left)
              ++ Chunk(ByteCode.Push(R0))
              ++ loop(right)
              ++ Chunk(ByteCode.Swap, ByteCode.Pop(R0), ByteCode.Div)
        }

      val code = optimise(loop(this))
      val ss   = calculateStackSize(code)
      Compiled(code.toArray, ss)
    }

    def compile: Compiled = synchronized {
      if (_compiled == null) {
        _compiled = _compile
      }

      _compiled
    }
  }

  case class Compiled(code: Array[ByteCode], stackSize: Int)

  object Op {
    case class GetLong(field: Field)    extends Op
    case class GetDouble(field: Field)  extends Op
    case class Add(left: Op, right: Op) extends Op
    case class Sub(left: Op, right: Op) extends Op
    case class Mul(left: Op, right: Op) extends Op
    case class Div(left: Op, right: Op) extends Op
  }

  final case class Dataset(private val texts: Map[Field, Array[String]], val longs: Map[Field, Array[Long]], doubles: Map[Field, Array[Double]]) {
    def execute(ops: Compiled): Either[Array[Long], Array[Double]] = {
      val rs: Array[Array[Long]]    = Array(null, null)
      val stack: Array[Array[Long]] = Array.ofDim(ops.stackSize)
      var sp                        = 0

      var i = 0
      val l = ops.code.length
      while (i < l) {
        val op = ops.code(i)
        (op.tag: @switch) match {
          case 0 =>
            val x = op.asInstanceOf[ByteCode.Get]
            rs(x.reg) = longs(x.field)
          case 1 =>
            val x = op.asInstanceOf[ByteCode.Push]
            stack(sp) = rs(x.value)
            sp = sp + 1
          case 2 =>
            val x = op.asInstanceOf[ByteCode.Pop]
            sp = sp - 1
            rs(x.value) = stack(sp)
          case 3 =>
            val tmp = rs(0)
            rs(0) = rs(1)
            rs(1) = tmp
          case 4 =>
            rs(0) = Dataset.combineLL(Dataset.plus, rs(0), rs(1))
          case 5 =>
            rs(0) = Dataset.combineLL(Dataset.minus, rs(0), rs(1))
          case 6 =>
            rs(0) = Dataset.combineLL(Dataset.times, rs(0), rs(1))
          case 7 =>
            rs(0) = Dataset.combineLL(Dataset.divide, rs(0), rs(1))
        }

        i = i + 1
      }

      Left(rs(0))
    }
  }

  object Dataset {
    private def combineLL(f: LongBinaryOperator, left: Array[Long], right: Array[Long]): Array[Long] = {
      val result = Array.fill[Long](left.length.max(right.length))(0L)
      var i      = 0
      val l      = left.length.min(right.length)
      while (i < l) {
        result(i) = f.applyAsLong(left(i), right(i))
        i += 1
      }
      result
    }

    private val plus: LongBinaryOperator   = _ + _
    private val minus: LongBinaryOperator  = _ - _
    private val times: LongBinaryOperator  = _ * _
    private val divide: LongBinaryOperator = _ / _

    def fromRows(rows: Chunk[Row]): Dataset = {

      val longs   = MutableMap.empty[Field, Array[Long]]
      val doubles = MutableMap.empty[Field, Array[Double]]
      val texts   = MutableMap.empty[Field, Array[String]]

      def updateColumns(field: Field, row: Row, rowIndex: Int): Unit = {

        val fieldValue: Value = row.map.getOrElse(field.name, null)
        if (fieldValue eq null) return

        fieldValue match {
          case Value.Text(value) =>
            val arr = texts.getOrElseUpdate(field, Array.fill(rows.length)(null))
            arr(rowIndex) = value
          case Value.Integer(value) =>
            val arr = longs.getOrElseUpdate(field, Array.fill(rows.length)(0L))
            arr(rowIndex) = value
          case Value.Decimal(value) =>
            val arr = doubles.getOrElseUpdate(field, Array.fill(rows.length)(0d))
            arr(rowIndex) = value
          case Value.NA =>
            ()
        }
      }

      for {
        (row, index) <- rows.zipWithIndex
        fieldName    <- row.map.keys
        field         = Field(fieldName)
      } yield updateColumns(field, row, index)

      new Dataset(texts.toMap, longs.toMap, doubles.toMap)
    }
  }
}
