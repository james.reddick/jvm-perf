package net.degoes

import scala.collection.mutable.Map as MutableMap
import zio.Chunk

object dataset4 {

  final case class Field(name: String)

  sealed trait Value
  object Value {
    final case class Text(value: String)    extends Value
    final case class Integer(value: Long)   extends Value
    final case class Decimal(value: Double) extends Value
    case object NA                          extends Value
  }

  final case class Row(map: Map[String, Value]) {
    def apply(field: Field): Value = map(field.name)
  }

  sealed trait Binary

  object Binary {
    case object ADD extends Binary
    case object SUB extends Binary
    case object MUL extends Binary
    case object DIV extends Binary
  }

  sealed trait Op {
    import Op.*

    def *(that: Op): Op =
      Mul(this, that)

    def /(that: Op): Op =
      Div(this, that)

    def +(that: Op): Op =
      Add(this, that)

    def -(that: Op): Op =
      Sub(this, that)
  }

  object Op {
    case class GetLong(field: Field)    extends Op
    case class GetDouble(field: Field)  extends Op
    case class Add(left: Op, right: Op) extends Op
    case class Sub(left: Op, right: Op) extends Op
    case class Mul(left: Op, right: Op) extends Op
    case class Div(left: Op, right: Op) extends Op
  }

  final case class Dataset(private val texts: Map[Field, Array[String]], val longs: Map[Field, Array[Long]], doubles: Map[Field, Array[Double]]) {
    def execute(op: Op): Either[Array[Long], Array[Double]] =
      op match {
        case x: Op.GetLong =>
          Left(longs(x.field))

        case x: Op.GetDouble =>
          Right(doubles(x.field))

        case x: Op.Mul =>
          (execute(x.left), execute(x.right)) match {
            case (Left(left), Left(right))   => Left(combineLL(_ * _, left, right))
            case (Left(left), Right(right))  => Right(combineLD(_ * _, left, right))
            case (Right(left), Left(right))  => Right(combineDL(_ * _, left, right))
            case (Right(left), Right(right)) => Right(combineDD(_ * _, left, right))
          }

        case x: Op.Add =>
          (execute(x.left), execute(x.right)) match {
            case (Left(left), Left(right))   => Left(combineLL(_ + _, left, right))
            case (Left(left), Right(right))  => Right(combineLD(_ + _, left, right))
            case (Right(left), Left(right))  => Right(combineDL(_ + _, left, right))
            case (Right(left), Right(right)) => Right(combineDD(_ + _, left, right))
          }

        case x: Op.Sub =>
          (execute(x.left), execute(x.right)) match {
            case (Left(left), Left(right))   => Left(combineLL(_ - _, left, right))
            case (Left(left), Right(right))  => Right(combineLD(_ - _, left, right))
            case (Right(left), Left(right))  => Right(combineDL(_ - _, left, right))
            case (Right(left), Right(right)) => Right(combineDD(_ - _, left, right))
          }

        case x: Op.Div =>
          (execute(x.left), execute(x.right)) match {
            case (Left(left), Left(right))   => Left(combineLL(_ / _, left, right))
            case (Left(left), Right(right))  => Right(combineLD(_ / _, left, right))
            case (Right(left), Left(right))  => Right(combineDL(_ / _, left, right))
            case (Right(left), Right(right)) => Right(combineDD(_ / _, left, right))
          }
      }

    private def combineLL(f: (Long, Long) => Long, left: Array[Long], right: Array[Long]): Array[Long] = {
      val result = Array.fill[Long](left.length.max(right.length))(0L)
      var i      = 0
      val l      = left.length.min(right.length)
      while (i < l) {
        result(i) = f(left(i), right(i))
        i += 1
      }

      result
    }

    private def combineLD(f: (Double, Double) => Double, left: Array[Long], right: Array[Double]): Array[Double] = {
      val result = Array.fill[Double](left.length.max(right.length))(0d)
      var i      = 0
      val l      = left.length.min(right.length)
      while (i < l) {
        result(i) = f(left(i).toDouble, right(i))
        i += 1
      }

      result
    }

    private def combineDL(f: (Double, Double) => Double, left: Array[Double], right: Array[Long]): Array[Double] = {
      val result = Array.fill[Double](left.length.max(right.length))(0d)
      var i      = 0
      val l      = left.length.min(right.length)
      while (i < l) {
        result(i) = f(left(i), right(i).toDouble)
        i += 1
      }

      result
    }

    private def combineDD(f: (Double, Double) => Double, left: Array[Double], right: Array[Double]): Array[Double] = {
      val result = Array.fill[Double](left.length.max(right.length))(0d)
      var i      = 0
      val l      = left.length.min(right.length)
      while (i < l) {
        result(i) = f(left(i), right(i))
        i += 1
      }

      result
    }
  }

  object Dataset {

    def fromRows(rows: Chunk[Row]): Dataset = {

      val longs   = MutableMap.empty[Field, Array[Long]]
      val doubles = MutableMap.empty[Field, Array[Double]]
      val texts   = MutableMap.empty[Field, Array[String]]

      def updateColumns(field: Field, row: Row, rowIndex: Int): Unit = {

        val fieldValue: Value = row.map.getOrElse(field.name, null)
        if (fieldValue eq null) return

        fieldValue match {
          case Value.Text(value) =>
            val arr = texts.getOrElseUpdate(field, Array.fill(rows.length)(null))
            arr(rowIndex) = value
          case Value.Integer(value) =>
            val arr = longs.getOrElseUpdate(field, Array.fill(rows.length)(0L))
            arr(rowIndex) = value
          case Value.Decimal(value) =>
            val arr = doubles.getOrElseUpdate(field, Array.fill(rows.length)(0d))
            arr(rowIndex) = value
          case Value.NA =>
            ()
        }
      }

      for {
        (row, index) <- rows.zipWithIndex
        fieldName    <- row.map.keys
        field         = Field(fieldName)
      } yield updateColumns(field, row, index)

      new Dataset(texts.toMap, longs.toMap, doubles.toMap)
    }
  }
}
